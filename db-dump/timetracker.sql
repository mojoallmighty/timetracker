-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 20 Feb 2020 la 01:33
-- Versiune server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `timetracker`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `timetracker_hours`
--

CREATE TABLE `timetracker_hours` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hours` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `create_date` varchar(100) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `timetracker_hours`
--

INSERT INTO `timetracker_hours` (`id`, `task_id`, `project_id`, `user_id`, `hours`, `description`, `create_date`, `created_at`, `updated_at`) VALUES
(5, 4, 16, 2, '2', 'Installed docker on VM', '1582150153', '2020-02-19 22:09:13', '2020-02-19 22:09:13'),
(6, 6, 18, 2, '3', 'Added indexes to db to optimize search', '1582150402', '2020-02-19 22:13:22', '2020-02-19 22:13:22'),
(7, 8, 16, 2, '5', 'Created angular docker to communicate with a server', '1582150860', '2020-02-19 22:21:00', '2020-02-19 22:21:00'),
(8, 7, 17, 3, '5', 'Added queries', '1582150904', '2020-02-19 22:21:44', '2020-02-19 22:21:44'),
(9, 5, 17, 5, '6', 'Made a go search', '1582150948', '2020-02-19 22:22:28', '2020-02-19 22:22:28');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `timetracker_projects`
--

CREATE TABLE `timetracker_projects` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` varchar(100) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `timetracker_projects`
--

INSERT INTO `timetracker_projects` (`id`, `name`, `description`, `created_by`, `create_date`, `created_at`, `updated_at`) VALUES
(16, 'Docker', 'Do some docker stuff', 1, '1582150027', '2020-02-19 22:07:07', '2020-02-19 22:07:07'),
(17, 'Google', 'Make a google based search engine', 2, '1582150132', '2020-02-19 22:08:52', '2020-02-19 22:08:52');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `timetracker_tasks`
--

CREATE TABLE `timetracker_tasks` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `deadline` varchar(100) DEFAULT NULL,
  `create_date` varchar(100) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `timetracker_tasks`
--

INSERT INTO `timetracker_tasks` (`id`, `project_id`, `name`, `description`, `created_by`, `deadline`, `create_date`, `created_at`, `updated_at`) VALUES
(4, 16, 'Install docker', 'Install docker and create a dockerfile', 1, NULL, '1582150027', '2020-02-19 22:07:07', '2020-02-19 22:07:07'),
(5, 17, 'GO Search', 'Create a simple search written in GO', 2, NULL, '1582150132', '2020-02-19 22:08:52', '2020-02-19 22:08:52'),
(6, 17, 'Optimize search engine', 'Optimize the search engine by adding indexes to db', 2, NULL, '1582150373', '2020-02-19 22:12:53', '2020-02-19 22:12:53'),
(7, 17, 'Add query', 'Add query to the search', 2, NULL, '1582150519', '2020-02-19 22:15:19', '2020-02-19 22:15:19'),
(8, 16, 'Create angular docker', 'Create angular docker to communicate with a Node server', 2, NULL, '1582150829', '2020-02-19 22:20:29', '2020-02-19 22:20:29');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `timetracker_users`
--

CREATE TABLE `timetracker_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `create_date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `timetracker_users`
--

INSERT INTO `timetracker_users` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `create_date`) VALUES
(1, 'test', 'Ion', 'Ion', 'test@test.test', 'S2ZVckJGWE9TSGM9', '1234'),
(2, 'test1', 'Vasile', 'Vasile', 'test2@test.test', 'S2ZVckJGWE9TSGM9', '1234'),
(3, 'test2', 'Gheorghe', 'Gheorghe', 'test3@test.test', 'S2ZVckJGWE9TSGM9', '1234'),
(4, 'test3', 'Ana', 'Ana', 'test4@test.test', 'S2ZVckJGWE9TSGM9', '1234'),
(5, 'test4', 'Maria', 'Maria', 'test5@test.test', 'S2ZVckJGWE9TSGM9', '1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `timetracker_hours`
--
ALTER TABLE `timetracker_hours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timetracker_projects`
--
ALTER TABLE `timetracker_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timetracker_tasks`
--
ALTER TABLE `timetracker_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timetracker_users`
--
ALTER TABLE `timetracker_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `timetracker_hours`
--
ALTER TABLE `timetracker_hours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `timetracker_projects`
--
ALTER TABLE `timetracker_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `timetracker_tasks`
--
ALTER TABLE `timetracker_tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `timetracker_users`
--
ALTER TABLE `timetracker_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
